Package: apt
Version: 1.3~rc2
Architecture: amd64
Multi-Arch: no
Priority: important
Depends: libapt-pkg5.0 (>= 1.3~rc2), libc6 (>= 2.15), libgcc1 (>= 1:3.0), libstdc++6 (>= 5.2), init-system-helpers (>= 1.18~), debian-archive-keyring, gpgv | gpgv2 | gpgv1, adduser
Breaks: apt-utils (<< 1.3~exp2~), manpages-it (<< 2.80-4~), manpages-pl (<< 20060617-3~), openjdk-6-jdk (<< 6b24-1.11-0ubuntu1~), sun-java5-jdk (>> 0), sun-java6-jdk (>> 0)
Suggests: aptitude | synaptic | wajig, dpkg-dev (>= 1.17.2), apt-doc, python-apt, powermgmt-base
Recommends: gnupg | gnupg2 | gnupg1
Replaces: apt-utils (<< 1.3~exp2~), bash-completion (<< 1:2.1-4.2+fakesync1), manpages-it (<< 2.80-4~), manpages-pl (<< 20060617-3~), openjdk-6-jdk (<< 6b24-1.11-0ubuntu1~), sun-java5-jdk (>> 0), sun-java6-jdk (>> 0)

Package: base-files
Version: 9.6
Architecture: amd64
Multi-Arch: foreign
Essential: yes
Priority: required
Provides: base
Pre-Depends: awk
Breaks: initscripts (<< 2.88dsf-13.3), sendfile (<< 2.1b.20080616-5.2~)
Replaces: base, dpkg (<= 1.15.0), miscutils

Package: base-passwd
Version: 3.5.40
Architecture: amd64
Multi-Arch: foreign
Essential: yes
Priority: required
Depends: libc6 (>= 2.8), libdebconfclient0 (>= 0.145)
Recommends: debconf (>= 0.5) | debconf-2.0
Replaces: base

Package: bash
Version: 4.3-15
Architecture: amd64
Multi-Arch: foreign
Essential: yes
Priority: required
Depends: base-files (>= 2.1.12), debianutils (>= 2.15)
Pre-Depends: dash (>= 0.5.5.1-2.2), libc6 (>= 2.15), libncurses5 (>= 6), libtinfo5 (>= 6)
Conflicts: bash-completion (<< 20060301-0)
Suggests: bash-doc
Recommends: bash-completion (>= 20060301-0)
Replaces: bash-completion (<< 20060301-0), bash-doc (<= 2.05-1)

Package: binutils
Version: 2.27-6
Architecture: amd64
Multi-Arch: no
Priority: optional
Provides: binutils-gold, elf-binutils
Depends: libc6 (>= 2.14), zlib1g (>= 1:1.2.0)
Conflicts: binutils-gold (<< 2.20.51.20100415), binutils-mingw-w64-i686 (<< 2.23.52.20130612-1+3), binutils-mingw-w64-x86-64 (<< 2.23.52.20130612-1+3), elf-binutils, gas, modutils (<< 2.4.19-1)
Breaks: binutils-mingw-w64-i686 (<< 2.23.52.20130612-1+3), binutils-mingw-w64-x86-64 (<< 2.23.52.20130612-1+3), hardening-wrapper (<< 2.8)
Suggests: binutils-doc (>= 2.27-6)
Replaces: binutils-gold (<< 2.20.51.20100415), binutils-mingw-w64-i686 (<< 2.23.52.20130612-1+3), binutils-mingw-w64-x86-64 (<< 2.23.52.20130612-1+3)

Package: bsdmainutils
Version: 9.0.10
Architecture: amd64
Multi-Arch: foreign
Priority: important
Depends: libbsd0 (>= 0.2.0), libc6 (>= 2.14), libncurses5 (>= 6), libtinfo5 (>= 6), bsdutils (>= 3.0-0), debianutils (>= 1.8)
Breaks: bsdutils (<< 1:2.13-11)
Suggests: cpp, wamerican | wordlist, whois, vacation

Package: bsdutils
Version: 1:2.28.1-1
Architecture: amd64
Multi-Arch: foreign
Essential: yes
Priority: required
Source: util-linux (2.28.1-1)
Pre-Depends: libc6 (>= 2.16), libsystemd0
Breaks: bash-completion (<< 1:2.1-4.1~)
Recommends: bsdmainutils
Replaces: bash-completion (<< 1:2.1-4.1~)

Package: build-essential
Version: 12.2
Architecture: amd64
Multi-Arch: no
Priority: optional
Depends: libc6-dev | libc-dev, gcc (>= 4:5.3), g++ (>= 4:5.3), make, dpkg-dev (>= 1.17.11)

Package: bzip2
Version: 1.0.6-8
Architecture: amd64
Multi-Arch: foreign
Priority: standard
Depends: libbz2-1.0 (= 1.0.6-8), libc6 (>= 2.14)
Suggests: bzip2-doc
Replaces: libbz2 (<< 0.9.5d-3)

Package: coreutils
Version: 8.25-2
Architecture: amd64
Multi-Arch: foreign
Essential: yes
Priority: required
Pre-Depends: libacl1 (>= 2.2.51-8), libattr1 (>= 1:2.4.46-8), libc6 (>= 2.17), libselinux1 (>= 2.1.13), multiarch-support
Conflicts: timeout
Replaces: mktemp, realpath, timeout

Package: cpp
Version: 4:6.1.1-1
Architecture: amd64
Multi-Arch: allowed
Priority: optional
Source: gcc-defaults (1.163)
Depends: cpp-6 (>= 6.1.1-9~)
Conflicts: cpp-doc (<< 1:2.95.3)
Suggests: cpp-doc

Package: cpp-6
Version: 6.2.0-1
Architecture: amd64
Multi-Arch: no
Priority: optional
Source: gcc-6
Depends: gcc-6-base (= 6.2.0-1), libc6 (>= 2.14), libgmp10 (>= 2:5.0.1~), libisl15 (>= 0.15), libmpc3, libmpfr4 (>= 3.1.3), zlib1g (>= 1:1.1.4)
Breaks: libmagics++-dev (<< 2.28.0-4)
Suggests: gcc-6-locales (>= 6.1.1-2)
Replaces: gccgo-6 (<< 6.2.0-1)

Package: dash
Version: 0.5.8-2.3
Architecture: amd64
Multi-Arch: no
Essential: yes
Priority: required
Depends: debianutils (>= 2.15), dpkg (>= 1.15.0)
Pre-Depends: libc6 (>= 2.14)

Package: debianutils
Version: 4.8
Architecture: amd64
Multi-Arch: foreign
Essential: yes
Priority: required
Depends: sensible-utils
Pre-Depends: libc6 (>= 2.15)
Replaces: manpages-pl (<< 1:0.5)

Package: diffutils
Version: 1:3.3-3
Architecture: amd64
Multi-Arch: no
Essential: yes
Priority: required
Pre-Depends: libc6 (>= 2.17)
Suggests: diffutils-doc, wdiff
Replaces: diff

Package: dpkg
Version: 1.18.10
Architecture: amd64
Multi-Arch: foreign
Essential: yes
Priority: required
Depends: tar (>= 1.28-1)
Pre-Depends: libbz2-1.0, libc6 (>= 2.14), liblzma5 (>= 5.1.1alpha+20120614), libselinux1 (>= 2.3), zlib1g (>= 1:1.1.4), tar (>= 1.23)
Breaks: dpkg-dev (<< 1.15.8), libdpkg-perl (<< 1.15.8)
Suggests: apt
Replaces: manpages-it (<< 2.80-4)

Package: e2fslibs
Version: 1.43.1-1
Architecture: amd64
Multi-Arch: same
Priority: required
Source: e2fsprogs
Provides: libe2p2, libext2fs2
Depends: libc6 (>= 2.17)
Replaces: e2fsprogs (<< 1.34-1)

Package: e2fsprogs
Version: 1.43.1-1
Architecture: amd64
Multi-Arch: foreign
Essential: yes
Priority: required
Pre-Depends: e2fslibs (= 1.43.1-1), libblkid1 (>= 2.17.2), libc6 (>= 2.14), libcomerr2 (>= 1.42~WIP-2011-10-05-1), libss2 (>= 1.34-1), libuuid1 (>= 2.16), util-linux (>= 2.15~rc1-1)
Conflicts: dump (<< 0.4b4-4), initscripts (<< 2.85-4), quota (<< 1.55-8.1), sysvinit (<< 2.85-4)
Suggests: gpart, parted, fuse2fs, e2fsck-static
Replaces: hurd (<= 20040301-1), libblkid1 (<< 1.38+1.39-WIP-2005.12.10-2), libuuid1 (<< 1.38+1.39-WIP-2005.12.10-2)

Package: file
Version: 1:5.28-4
Architecture: amd64
Multi-Arch: foreign
Priority: standard
Depends: libc6 (>= 2.4), libmagic1 (= 1:5.28-4), zlib1g (>= 1:1.1.4)

Package: findutils
Version: 4.6.0+git+20160703-2
Architecture: amd64
Multi-Arch: foreign
Essential: yes
Priority: required
Pre-Depends: libc6 (>= 2.17), libselinux1 (>= 1.32)
Conflicts: debconf (<< 1.5.50)
Breaks: binstats (<< 1.08-8.1), debhelper (<< 9.20130504), guilt (<< 0.36-0.2), kernel-package (<< 13.000), libpython3.4-minimal (<< 3.4.4-2), libpython3.5-minimal (<< 3.5.1-3), lsat (<< 0.9.7.1-2.1), mc (<< 3:4.8.11-1), sendmail (<< 8.14.4-5), switchconf (<< 0.0.9-2.1)
Suggests: mlocate | locate

Package: g++
Version: 4:6.1.1-1
Architecture: amd64
Multi-Arch: no
Priority: optional
Source: gcc-defaults (1.163)
Provides: c++-compiler
Depends: cpp (>= 4:6.1.1-1), gcc (>= 4:6.1.1-1), g++-6 (>= 6.1.1-9~), gcc-6 (>= 6.1.1-9~)
Suggests: g++-multilib

Package: g++-6
Version: 6.2.0-1
Architecture: amd64
Multi-Arch: no
Priority: optional
Source: gcc-6
Provides: c++-compiler, c++abi2-dev
Depends: gcc-6-base (= 6.2.0-1), gcc-6 (= 6.2.0-1), libstdc++-6-dev (= 6.2.0-1), libc6 (>= 2.14), libgmp10 (>= 2:5.0.1~), libisl15 (>= 0.15), libmpc3, libmpfr4 (>= 3.1.3), zlib1g (>= 1:1.1.4)
Suggests: g++-6-multilib, gcc-6-doc (>= 6.1.1-2), libstdc++6-6-dbg (>= 6.2.0-1)

Package: gawk
Version: 1:4.1.3+dfsg-0.1
Architecture: amd64
Multi-Arch: foreign
Priority: optional
Provides: awk
Pre-Depends: libc6 (>= 2.15), libgmp10, libmpfr4 (>= 3.1.3), libreadline6 (>= 6.0), libsigsegv2 (>= 2.9)
Suggests: gawk-doc

Package: gcc
Version: 4:6.1.1-1
Architecture: amd64
Multi-Arch: no
Priority: optional
Source: gcc-defaults (1.163)
Provides: c-compiler
Depends: cpp (>= 4:6.1.1-1), gcc-6 (>= 6.1.1-9~)
Conflicts: gcc-doc (<< 1:2.95.3)
Suggests: gcc-multilib, make, manpages-dev, autoconf, automake, libtool, flex, bison, gdb, gcc-doc
Recommends: libc6-dev | libc-dev

Package: gcc-6
Version: 6.2.0-1
Architecture: amd64
Multi-Arch: no
Priority: optional
Provides: c-compiler
Depends: cpp-6 (= 6.2.0-1), gcc-6-base (= 6.2.0-1), libcc1-0 (>= 6.2.0-1), binutils (>= 2.27), libgcc-6-dev (= 6.2.0-1), libc6 (>= 2.14), libgcc1 (>= 1:3.0), libgmp10 (>= 2:5.0.1~), libisl15 (>= 0.15), libmpc3, libmpfr4 (>= 3.1.3), libstdc++6 (>= 5), zlib1g (>= 1:1.1.4)
Suggests: gcc-6-multilib, gcc-6-doc (>= 6.1.1-2), gcc-6-locales (>= 6.1.1-2), libgcc1-dbg (>= 1:6.2.0-1), libgomp1-dbg (>= 6.2.0-1), libitm1-dbg (>= 6.2.0-1), libatomic1-dbg (>= 6.2.0-1), libasan3-dbg (>= 6.2.0-1), liblsan0-dbg (>= 6.2.0-1), libtsan0-dbg (>= 6.2.0-1), libubsan0-dbg (>= 6.2.0-1), libcilkrts5-dbg (>= 6.2.0-1), libmpx2-dbg (>= 6.2.0-1), libquadmath0-dbg (>= 6.2.0-1)
Recommends: libc6-dev (>= 2.13-5)
Replaces: gccgo-6 (<< 6.2.0-1)

Package: gcc-6-base
Version: 6.2.0-1
Architecture: amd64
Multi-Arch: same
Priority: required
Source: gcc-6
Breaks: gcc-4.4-base (<< 4.4.7), gcc-4.7-base (<< 4.7.3), gcj-4.4-base (<< 4.4.6-9~), gcj-4.6-base (<< 4.6.1-4~), gnat-4.4-base (<< 4.4.6-3~), gnat-4.6 (<< 4.6.1-5~)

Package: gettext
Version: 0.19.8.1-1
Architecture: amd64
Multi-Arch: foreign
Priority: optional
Depends: libc6 (>= 2.17), libcroco3 (>= 0.6.2), libglib2.0-0 (>= 2.12.0), libgomp1 (>= 4.9), libncurses5 (>= 6), libtinfo5 (>= 6), libunistring0, libxml2 (>= 2.9.1), gettext-base, dpkg (>= 1.15.4) | install-info
Breaks: autopoint (<= 0.17-11)
Suggests: gettext-doc, autopoint, libasprintf-dev, libgettextpo-dev
Recommends: curl | wget | lynx-cur

Package: gettext-base
Version: 0.19.8.1-1
Architecture: amd64
Multi-Arch: foreign
Priority: standard
Source: gettext
Depends: libc6 (>= 2.14)

Package: gpgv
Version: 2.1.14-5
Architecture: amd64
Multi-Arch: foreign
Priority: important
Source: gnupg2
Depends: libbz2-1.0, libc6 (>= 2.14), libgcrypt20 (>= 1.7.0), libgpg-error0 (>= 1.14), zlib1g (>= 1:1.1.4)
Breaks: gnupg2 (<< 2.0.21-2), gpgv2 (<< 2.1.11-7+exp1)
Suggests: gnupg
Replaces: gnupg2 (<< 2.0.21-2), gpgv2 (<< 2.1.11-7+exp1)

Package: grep
Version: 2.25-6
Architecture: amd64
Multi-Arch: foreign
Essential: yes
Priority: required
Provides: rgrep
Depends: dpkg (>= 1.15.4) | install-info
Pre-Depends: libc6 (>= 2.14), libpcre3
Conflicts: rgrep
Suggests: libpcre3 (>= 7.7)

Package: groff-base
Version: 1.22.3-8
Architecture: amd64
Multi-Arch: foreign
Priority: standard
Source: groff
Depends: libc6 (>= 2.14), libgcc1 (>= 1:3.0), libstdc++6 (>= 4.1.1)
Breaks: groff (<< 1.17-1), jgroff (<< 1.17-1), pmake (<< 1.45-7), troffcvt (<< 1.04-14)
Suggests: groff
Replaces: groff (<< 1.20.1-6), jgroff (<< 1.17-1)

Package: guile-2.0-libs
Version: 2.0.11+1-12
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: guile-2.0
Provides: guile-2.0-slib
Depends: libc6 (>= 2.17), libffi6 (>= 3.0.4), libgc1c2 (>= 1:7.2d), libgmp10, libltdl7 (>= 2.4.6), libncurses5 (>= 6), libreadline6 (>= 6.0), libtinfo5 (>= 6), libunistring0
Conflicts: guile-2.0-slib

Package: gzip
Version: 1.6-5
Architecture: amd64
Multi-Arch: no
Essential: yes
Priority: required
Depends: dpkg (>= 1.15.4) | install-info
Pre-Depends: libc6 (>= 2.17)
Suggests: less

Package: hostname
Version: 3.18
Architecture: amd64
Multi-Arch: no
Essential: yes
Priority: required
Pre-Depends: libc6 (>= 2.4)
Breaks: nis (<< 3.17-30)
Replaces: nis (<< 3.17-30)

Package: libacl1
Version: 2.2.52-3
Architecture: amd64
Multi-Arch: same
Priority: required
Source: acl
Depends: libattr1 (>= 1:2.4.46-8), libc6 (>= 2.14)
Conflicts: acl (<< 2.0.0), libacl1-kerberos4kth

Package: libapt-pkg5.0
Version: 1.3~rc2
Architecture: amd64
Multi-Arch: same
Priority: important
Source: apt
Provides: libapt-pkg (= 1.3~rc2)
Depends: libbz2-1.0, libc6 (>= 2.15), libgcc1 (>= 1:3.0), liblz4-1 (>= 0.0~r127), liblzma5 (>= 5.1.1alpha+20120614), libstdc++6 (>= 5.2), zlib1g (>= 1:1.2.2.3)
Breaks: appstream (<< 0.9.0-3~), apt (<< 1.1~exp14), libapt-inst1.5 (<< 0.9.9~)
Recommends: apt (>= 1.3~rc2)

Package: libasan3
Version: 6.2.0-1
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: gcc-6
Depends: gcc-6-base (= 6.2.0-1), libc6 (>= 2.23), libgcc1 (>= 1:3.3), libstdc++6 (>= 4.1.1)

Package: libatomic1
Version: 6.2.0-1
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: gcc-6
Depends: gcc-6-base (= 6.2.0-1), libc6 (>= 2.14)

Package: libattr1
Version: 1:2.4.47-2
Architecture: amd64
Multi-Arch: same
Priority: required
Source: attr
Depends: libc6 (>= 2.4)
Pre-Depends: multiarch-support
Conflicts: attr (<< 2.0.0)

Package: libaudit1
Version: 1:2.6.6-1
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: audit
Depends: libaudit-common (>= 1:2.6.6-1), libc6 (>= 2.14), libcap-ng0

Package: libblkid1
Version: 2.28.1-1
Architecture: amd64
Multi-Arch: same
Priority: required
Source: util-linux
Depends: libc6 (>= 2.17), libuuid1 (>= 2.16)

Package: libbsd0
Version: 0.8.3-1
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: libbsd
Depends: libc6 (>= 2.16)

Package: libbz2-1.0
Version: 1.0.6-8
Architecture: amd64
Multi-Arch: same
Priority: important
Source: bzip2
Depends: libc6 (>= 2.4)

Package: libc-bin
Version: 2.23-5
Architecture: amd64
Multi-Arch: foreign
Essential: yes
Priority: required
Source: glibc
Depends: libc6 (>> 2.23), libc6 (<< 2.24)
Recommends: manpages

Package: libc-dev-bin
Version: 2.23-5
Architecture: amd64
Multi-Arch: foreign
Priority: optional
Source: glibc
Depends: libc6 (>> 2.23), libc6 (<< 2.24)
Recommends: manpages, manpages-dev

Package: libc6
Version: 2.23-5
Architecture: amd64
Multi-Arch: same
Priority: required
Source: glibc
Depends: libgcc1
Breaks: hurd (<< 1:0.5.git20140203-1), libtirpc1 (<< 0.2.3), locales (<< 2.23), locales-all (<< 2.23), lsb-core (<= 3.2-27), nscd (<< 2.23)
Suggests: glibc-doc, debconf | debconf-2.0, libc-l10n, locales
Replaces: libc6-amd64

Package: libc6-dev
Version: 2.23-5
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: glibc
Provides: libc-dev
Depends: libc6 (= 2.23-5), libc-dev-bin (= 2.23-5), linux-libc-dev
Conflicts: libc0.1-dev, libc0.3-dev, libc6.1-dev
Breaks: binutils (<< 2.26), binutils-gold (<< 2.20.1-11), cmake (<< 2.8.4+dfsg.1-5), gcc-4.4 (<< 4.4.6-4), gcc-4.5 (<< 4.5.3-2), gcc-4.6 (<< 4.6.0-12), libhwloc-dev (<< 1.2-3), libjna-java (<< 3.2.7-4), liblouis-dev (<< 2.3.0-2), liblouisxml-dev (<< 2.4.0-2), make (<< 3.81-8.1), pkg-config (<< 0.26-1)
Suggests: glibc-doc, manpages-dev

Package: libcap-ng0
Version: 0.7.7-3
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: libcap-ng
Depends: libc6 (>= 2.8)

Package: libcc1-0
Version: 6.2.0-1
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: gcc-6
Depends: gcc-6-base (= 6.2.0-1), libc6 (>= 2.14), libgcc1 (>= 1:3.0), libstdc++6 (>= 5.2)

Package: libcilkrts5
Version: 6.2.0-1
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: gcc-6
Depends: gcc-6-base (= 6.2.0-1), libc6 (>= 2.14), libgcc1 (>= 1:3.0), libstdc++6 (>= 4.1.1)

Package: libcomerr2
Version: 1.43.1-1
Architecture: amd64
Multi-Arch: same
Priority: required
Source: e2fsprogs
Provides: libcomerr-kth-compat
Depends: libc6 (>= 2.17)
Replaces: e2fsprogs (<< 1.34-1)

Package: libcroco3
Version: 0.6.11-1
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: libcroco
Depends: libc6 (>= 2.14), libglib2.0-0 (>= 2.16.0), libxml2 (>= 2.7.4)

Package: libdb5.3
Version: 5.3.28-12
Architecture: amd64
Multi-Arch: same
Priority: standard
Source: db5.3
Depends: libc6 (>= 2.17)

Package: libdebconfclient0
Version: 0.215
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: cdebconf
Depends: libc6 (>= 2.4)

Package: libfdisk1
Version: 2.28.1-1
Architecture: amd64
Multi-Arch: same
Priority: required
Source: util-linux
Depends: libblkid1 (>= 2.24.2), libc6 (>= 2.17), libuuid1 (>= 2.16)

Package: libffi6
Version: 3.2.1-4
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: libffi
Depends: libc6 (>= 2.14)

Package: libgc1c2
Version: 1:7.4.2-8
Architecture: amd64
Multi-Arch: same
Priority: standard
Source: libgc
Depends: libc6 (>= 2.14), libgcc1 (>= 1:3.0), libstdc++6 (>= 4.1.1)
Conflicts: libgc1
Replaces: libgc1, libgc1c3

Package: libgcc-6-dev
Version: 6.2.0-1
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: gcc-6
Depends: gcc-6-base (= 6.2.0-1), libgcc1 (>= 1:6.2.0-1), libgomp1 (>= 6.2.0-1), libitm1 (>= 6.2.0-1), libatomic1 (>= 6.2.0-1), libasan3 (>= 6.2.0-1), liblsan0 (>= 6.2.0-1), libtsan0 (>= 6.2.0-1), libubsan0 (>= 6.2.0-1), libcilkrts5 (>= 6.2.0-1), libmpx2 (>= 6.2.0-1), libquadmath0 (>= 6.2.0-1)
Recommends: libc6-dev (>= 2.13-5)
Replaces: gccgo-6 (<< 6.2.0-1)

Package: libgcc1
Version: 1:6.2.0-1
Architecture: amd64
Multi-Arch: same
Priority: required
Source: gcc-6 (6.2.0-1)
Depends: gcc-6-base (= 6.2.0-1), libc6 (>= 2.14)
Breaks: gcc-4.3 (<< 4.3.6-1), gcc-4.4 (<< 4.4.6-4), gcc-4.5 (<< 4.5.3-2)

Package: libgcrypt20
Version: 1.7.3-1
Architecture: amd64
Multi-Arch: same
Priority: standard
Depends: libc6 (>= 2.15), libgpg-error0 (>= 1.14)
Suggests: rng-tools

Package: libgdbm3
Version: 1.8.3-14
Architecture: amd64
Multi-Arch: same
Priority: important
Source: gdbm
Depends: libc6 (>= 2.14), dpkg (>= 1.15.4) | install-info

Package: libglib2.0-0
Version: 2.48.1-3
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: glib2.0
Depends: libc6 (>= 2.17), libffi6 (>= 3.0.4), libpcre3, libselinux1 (>= 1.32), zlib1g (>= 1:1.2.2)
Breaks: glib-networking (<< 2.33.12), libgnome-desktop-3-2 (<< 3.4.2-2), python-gi (<< 3.7.2)
Recommends: libglib2.0-data, shared-mime-info, xdg-user-dirs

Package: libgmp10
Version: 2:6.1.1+dfsg-1
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: gmp
Depends: libc6 (>= 2.14)

Package: libgomp1
Version: 6.2.0-1
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: gcc-6
Depends: gcc-6-base (= 6.2.0-1), libc6 (>= 2.17)
Breaks: gcc-4.3 (<< 4.3.6-1), gcc-4.4 (<< 4.4.6-4), gcc-4.5 (<< 4.5.3-2)

Package: libgpg-error0
Version: 1.24-1
Architecture: amd64
Multi-Arch: same
Priority: standard
Source: libgpg-error
Depends: libc6 (>= 2.15)

Package: libicu57
Version: 57.1-3
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: icu
Depends: libc6 (>= 2.14), libgcc1 (>= 1:3.0), libstdc++6 (>= 5.2)

Package: libisl15
Version: 0.17.1-1
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: isl
Depends: libc6 (>= 2.14), libgmp10

Package: libitm1
Version: 6.2.0-1
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: gcc-6
Depends: gcc-6-base (= 6.2.0-1), libc6 (>= 2.14)

Package: liblsan0
Version: 6.2.0-1
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: gcc-6
Depends: gcc-6-base (= 6.2.0-1), libc6 (>= 2.3), libgcc1 (>= 1:3.3), libstdc++6 (>= 4.1.1)

Package: libltdl7
Version: 2.4.6-2
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: libtool
Depends: libc6 (>= 2.14)

Package: liblz4-1
Version: 0.0~r131-2
Architecture: amd64
Multi-Arch: same
Priority: extra
Source: lz4
Depends: libc6 (>= 2.14)

Package: liblzma5
Version: 5.1.1alpha+20120614-2.1
Architecture: amd64
Multi-Arch: same
Priority: required
Source: xz-utils
Depends: libc6 (>= 2.14)

Package: libmagic-mgc
Version: 1:5.28-4
Architecture: amd64
Multi-Arch: foreign
Priority: standard
Source: file
Breaks: libmagic1 (<< 1:5.28-4~)
Replaces: libmagic1 (<< 1:5.28-4~)

Package: libmagic1
Version: 1:5.28-4
Architecture: amd64
Multi-Arch: same
Priority: standard
Source: file
Depends: libc6 (>= 2.15), zlib1g (>= 1:1.1.4), libmagic-mgc (= 1:5.28-4)
Suggests: file

Package: libmount1
Version: 2.28.1-1
Architecture: amd64
Multi-Arch: same
Priority: required
Source: util-linux
Depends: libblkid1 (>= 2.17.2), libc6 (>= 2.17), libselinux1 (>= 1.32)

Package: libmpc3
Version: 1.0.3-1
Architecture: amd64
Multi-Arch: same
Priority: extra
Source: mpclib3
Depends: libc6 (>= 2.4), libgmp10, libmpfr4 (>= 3.1.2)
Pre-Depends: multiarch-support

Package: libmpfr4
Version: 3.1.4-2
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: mpfr4
Depends: libc6 (>= 2.14), libgmp10
Breaks: libgmp3 (<< 4.1.4-3)

Package: libmpx2
Version: 6.2.0-1
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: gcc-6
Depends: gcc-6-base (= 6.2.0-1), libc6 (>= 2.17)
Replaces: libmpx0 (<< 6-20160120-1)

Package: libncurses5
Version: 6.0+20160625-1
Architecture: amd64
Multi-Arch: same
Priority: required
Source: ncurses
Depends: libtinfo5 (= 6.0+20160625-1), libc6 (>= 2.14)
Recommends: libgpm2

Package: libncursesw5
Version: 6.0+20160625-1
Architecture: amd64
Multi-Arch: same
Priority: important
Source: ncurses
Depends: libtinfo5 (= 6.0+20160625-1), libc6 (>= 2.14)
Recommends: libgpm2

Package: libpam-modules
Version: 1.1.8-3.3
Architecture: amd64
Multi-Arch: same
Priority: required
Source: pam
Provides: libpam-mkhomedir, libpam-motd, libpam-umask
Pre-Depends: libaudit1 (>= 1:2.2.1), libc6 (>= 2.15), libdb5.3, libpam0g (>= 1.1.3-2), libselinux1 (>= 2.1.9), debconf (>= 0.5) | debconf-2.0, libpam-modules-bin (= 1.1.8-3.3)
Conflicts: libpam-mkhomedir, libpam-motd, libpam-umask
Replaces: libpam-umask, libpam0g-util

Package: libpam-modules-bin
Version: 1.1.8-3.3
Architecture: amd64
Multi-Arch: foreign
Priority: required
Source: pam
Depends: libaudit1 (>= 1:2.2.1), libc6 (>= 2.14), libpam0g (>= 0.99.7.1), libselinux1 (>= 1.32)
Replaces: libpam-modules (<< 1.1.3-8)

Package: libpam0g
Version: 1.1.8-3.3
Architecture: amd64
Multi-Arch: same
Priority: required
Source: pam
Depends: libaudit1 (>= 1:2.2.1), libc6 (>= 2.14), debconf (>= 0.5) | debconf-2.0
Suggests: libpam-doc
Replaces: libpam0g-util

Package: libpcre3
Version: 2:8.39-2
Architecture: amd64
Multi-Arch: same
Priority: required
Source: pcre3
Depends: libc6 (>= 2.14)
Pre-Depends: multiarch-support
Conflicts: libpcre3-dev (<= 4.3-3)
Breaks: approx (<< 4.4-1~), cduce (<< 0.5.3-2~), cmigrep (<< 1.5-7~), galax (<< 1.1-7~), libpcre-ocaml (<< 6.0.1~), liquidsoap (<< 0.9.2-3~), ocsigen (<< 1.3.3-1~)

Package: libperl5.22
Version: 5.22.2-3
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: perl
Provides: libcompress-raw-bzip2-perl, libcompress-raw-zlib-perl, libcompress-zlib-perl, libdigest-md5-perl, libdigest-sha-perl, libencode-perl, libio-compress-base-perl, libio-compress-bzip2-perl, libio-compress-perl, libio-compress-zlib-perl, libmime-base64-perl, libstorable-perl, libsys-syslog-perl, libthreads-perl, libthreads-shared-perl, libtime-hires-perl, libtime-piece-perl, perl-cross-config
Depends: libbz2-1.0, libc6 (>= 2.23), libdb5.3, libgdbm3 (>= 1.8.3), zlib1g (>= 1:1.2.2.3), perl-modules-5.22 (>= 5.22.2-3)
Breaks: dh-make-perl (<< 0.73-1), ftpmirror (<< 1.96+dfsg-13), libcompress-raw-bzip2-perl (<< 2.068), libcompress-raw-zlib-perl (<< 2.068), libcompress-zlib-perl (<< 2.068), libdigest-md5-perl (<< 2.54), libdigest-sha-perl (<< 5.95), libencode-perl (<< 2.77), libhtml-template-compiled-perl (<< 0.95-1), libio-compress-base-perl (<< 2.068), libio-compress-bzip2-perl (<< 2.068), libio-compress-perl (<< 2.068), libio-compress-zlib-perl (<< 2.068), libload-perl (<< 0.20-1), libmime-base64-perl (<< 3.15), libnet-jifty-perl (<< 0.14-1), libperl-apireference-perl (<< 0.09-1), libregexp-optimizer-perl (<< 0.15-3), libsoap-lite-perl (<< 0.712-4), libstorable-perl (<< 2.53.01), libsys-syslog-perl (<< 0.33), libthreads-perl (<< 2.01), libthreads-shared-perl (<< 1.48), libtime-hires-perl (<< 1.9726), libtime-piece-perl (<< 1.29), libxml-parser-lite-tree-perl (<< 0.14-1), libyaml-perl (<< 0.73-1), mrtg (<< 2.16.3-3.1)
Replaces: libarchive-tar-perl (<= 1.38-2), libcompress-raw-bzip2-perl (<< 2.068), libcompress-raw-zlib-perl (<< 2.068), libcompress-zlib-perl (<< 2.068), libdigest-md5-perl (<< 2.54), libdigest-sha-perl (<< 5.95), libencode-perl (<< 2.77), libio-compress-base-perl (<< 2.068), libio-compress-bzip2-perl (<< 2.068), libio-compress-perl (<< 2.068), libio-compress-zlib-perl (<< 2.068), libmime-base64-perl (<< 3.15), libmodule-corelist-perl (<< 2.14-2), libstorable-perl (<< 2.53.01), libsys-syslog-perl (<< 0.33), libthreads-perl (<< 2.01), libthreads-shared-perl (<< 1.48), libtime-hires-perl (<< 1.9726), libtime-piece-perl (<< 1.29), perl (<< 5.22.0~), perl-base (<< 5.22.0~)

Package: libpipeline1
Version: 1.4.1-2
Architecture: amd64
Multi-Arch: same
Priority: important
Source: libpipeline
Depends: libc6 (>= 2.15)

Package: libquadmath0
Version: 6.2.0-1
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: gcc-6
Depends: gcc-6-base (= 6.2.0-1), libc6 (>= 2.23)

Package: libreadline6
Version: 6.3-8+b4
Architecture: amd64
Multi-Arch: same
Priority: important
Source: readline6 (6.3-8)
Depends: readline-common, libc6 (>= 2.15), libtinfo5 (>= 6)

Package: libselinux1
Version: 2.5-3
Architecture: amd64
Multi-Arch: same
Priority: required
Source: libselinux
Depends: libc6 (>= 2.14), libpcre3

Package: libsemanage1
Version: 2.5-1
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: libsemanage
Depends: libsemanage-common (= 2.5-1), libaudit1 (>= 1:2.2.1), libbz2-1.0, libc6 (>= 2.14), libselinux1 (>= 2.1.12), libsepol1 (>= 2.5), libustr-1.0-1 (>= 1.0.4)
Breaks: policycoreutils (<< 2.4), selinux-policy-default (<< 2:2.20140421-10~), selinux-policy-mls (<< 2:2.20140421-10~)

Package: libsepol1
Version: 2.5-1
Architecture: amd64
Multi-Arch: same
Priority: required
Source: libsepol
Depends: libc6 (>= 2.14)

Package: libsigsegv2
Version: 2.10-5
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: libsigsegv
Depends: libc6 (>= 2.4)

Package: libsmartcols1
Version: 2.28.1-1
Architecture: amd64
Multi-Arch: same
Priority: required
Source: util-linux
Depends: libc6 (>= 2.17)

Package: libss2
Version: 1.43.1-1
Architecture: amd64
Multi-Arch: same
Priority: required
Source: e2fsprogs
Depends: libcomerr2, libc6 (>= 2.17)
Replaces: e2fsprogs (<< 1.34-1)

Package: libstdc++-6-dev
Version: 6.2.0-1
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: gcc-6
Provides: libstdc++-dev
Depends: gcc-6-base (= 6.2.0-1), libgcc-6-dev (= 6.2.0-1), libstdc++6 (>= 6.2.0-1), libc6-dev (>= 2.13-5)
Conflicts: libg++2.8-dev, libg++27-dev, libg++272-dev (<< 2.7.2.8-1), libstdc++2.10-dev (<< 1:2.95.3-2), libstdc++2.8-dev, libstdc++2.9-dev, libstdc++2.9-glibc2.1-dev, libstdc++3.0-dev
Suggests: libstdc++-6-doc

Package: libstdc++6
Version: 6.2.0-1
Architecture: amd64
Multi-Arch: same
Priority: important
Source: gcc-6
Depends: gcc-6-base (= 6.2.0-1), libc6 (>= 2.18), libgcc1 (>= 1:4.2)
Conflicts: scim (<< 1.4.2-1)
Breaks: blockattack (<= 1.4.1+ds1-2.1+b2), boo (<= 0.9.5~git20110729.r1.202a430-2), c++-annotations (<= 10.2.0-1), clustalx (<= 2.1+lgpl-3), dff (<= 1.3.0+dfsg.1-4.1+b3), digikam-private-libs (<= 4:4.4.0-1.1+b2), emscripten (<= 1.22.1-1), ergo (<= 3.4.0-1), fceux (<= 2.2.2+dfsg0-1), fiona (<= 1.5.1-2), flush (<= 0.9.12-3.1), freeorion (<= 0.4.4+git20150327-2), fslview (<= 4.0.1-4), fwbuilder (<= 5.1.0-4), gcc-4.3 (<< 4.3.6-1), gcc-4.4 (<< 4.4.6-4), gcc-4.5 (<< 4.5.3-2), gnote (<= 3.16.2-1), gnudatalanguage (<= 0.9.5-2+b2), innoextract (<= 1.4-1+b1), libantlr-dev (<= 2.7.7+dfsg-6), libapache2-mod-passenger (<= 5.0.7-1), libaqsis1 (<= 1.8.2-1), libassimp3 (<= 3.0~dfsg-4), libboost-date-time1.54.0, libboost-date-time1.55.0, libchemps2-1 (<= 1.5-1), libcpprest2.4 (<= 2.4.0-2), libdap17 (<= 3.14.0-2), libdapclient6 (<= 3.14.0-2), libdapserver7 (<= 3.14.0-2), libdavix0 (<= 0.4.0-1+b1), libdballe6 (<= 6.8-1), libdiet-admin2.8 (<= 2.8.0-1+b3), libdiet-client2.8 (<= 2.8.0-1+b3), libdiet-sed2.8 (<= 2.8.0-1+b3), libfreefem++ (<= 3.37.1-1), libgazebo5 (<= 5.0.1+dfsg-2.1), libgetfem4++ (<= 4.2.1~beta1~svn4635~dfsg-3+b1), libgmsh2 (<= 2.9.3+dfsg1-1), libinsighttoolkit4.7 (<= 4.7.2-2), libkolabxml1 (<= 1.1.0-3), libmarisa0 (<= 0.2.4-8), libogre-1.8.0 (<= 1.8.0+dfsg1-7+b1), libogre-1.9.0 (<= 1.9.0+dfsg1-4), libopenwalnut1 (<= 1.4.0~rc1+hg3a3147463ee2-1+b1), libpqxx-4.0 (<= 4.0.1+dfsg-3), libreoffice-core (<= 1:4.4.5-2), librime1 (<= 1.2+dfsg-2), libwibble-dev (<= 1.1-1), libwreport2 (<= 2.14-1), libxmltooling6 (<= 1.5.3-2.1), lightspark (<= 0.7.2+git20150512-2+b1), mira-assembler (<= 4.9.5-1), mongodb (<= 1:2.4.14-2), mongodb-server (<= 1:2.4.14-2), ncbi-blast+ (<= 2.2.30-4), openscad (<= 2014.03+dfsg-1+b1), passepartout (<= 0.7.1-1.1), pdf2djvu (<= 0.7.21-2), photoprint (<= 0.4.2~pre2-2.3+b2), plastimatch (<= 1.6.2+dfsg-1), plee-the-bear (<= 0.6.0-3.1), povray (<= 1:3.7.0.0-8), powertop (<= 2.6.1-1), printer-driver-brlaser (<= 3-3), psi4 (<= 4.0~beta5+dfsg-2+b1), python-fiona (<= 1.5.1-2), python-guiqwt (<= 2.3.1-1), python-healpy (<= 1.8.1-1+b1), python-htseq (<= 0.5.4p3-2), python-imposm (<= 2.5.0-3+b2), python-pysph (<= 0~20150606.gitfa26de9-5), python-rasterio (<= 0.24.0-1), python-scipy (<= 0.14.1-1), python-sfml (<= 2.2~git20150611.196c88+dfsg-1+b1), python3-fiona (<= 1.5.1-2), python3-scipy (<= 0.14.1-1), python3-sfml (<= 2.2~git20150611.196c88+dfsg-1+b1), python3-taglib (<= 0.3.6+dfsg-2+b2), realtimebattle (<= 1.0.8-14), ruby-passenger (<= 5.0.7-1), schroot (<= 1.6.10-1+b1), sqlitebrowser (<= 3.5.1-3), tecnoballz (<= 0.93.1-6), wesnoth-1.12-core (<= 1:1.12.4-1), widelands (<= 1:18-3+b1), xflr5 (<= 6.09.06-2)
Replaces: libstdc++6-6-dbg (<< 4.9.0-3)

Package: libsystemd0
Version: 231-5
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: systemd
Pre-Depends: libc6 (>= 2.17), libgcrypt20 (>= 1.7.0), liblzma5 (>= 5.1.1alpha+20120614), libselinux1 (>= 1.32)

Package: libtinfo5
Version: 6.0+20160625-1
Architecture: amd64
Multi-Arch: same
Priority: required
Source: ncurses
Depends: libc6 (>= 2.16)
Breaks: dialog (<< 1.2-20130523)
Replaces: libncurses5 (<< 5.9-3)

Package: libtsan0
Version: 6.2.0-1
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: gcc-6
Depends: gcc-6-base (= 6.2.0-1), libc6 (>= 2.23), libgcc1 (>= 1:3.3), libstdc++6 (>= 4.1.1)

Package: libubsan0
Version: 6.2.0-1
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: gcc-6
Depends: gcc-6-base (= 6.2.0-1), libc6 (>= 2.2.5), libgcc1 (>= 1:3.3), libstdc++6 (>= 4.1.1)

Package: libudev1
Version: 231-5
Architecture: amd64
Multi-Arch: same
Priority: important
Source: systemd
Depends: libc6 (>= 2.16)

Package: libunistring0
Version: 0.9.6+really0.9.3-0.1
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: libunistring
Depends: libc6 (>= 2.14)

Package: libustr-1.0-1
Version: 1.0.4-5
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: ustr
Depends: libc6 (>= 2.14)

Package: libuuid1
Version: 2.28.1-1
Architecture: amd64
Multi-Arch: same
Priority: required
Source: util-linux
Depends: passwd, libc6 (>= 2.4)
Recommends: uuid-runtime
Replaces: e2fsprogs (<< 1.34-1)

Package: libxml2
Version: 2.9.4+dfsg1-1+b1
Architecture: amd64
Multi-Arch: same
Priority: standard
Source: libxml2 (2.9.4+dfsg1-1)
Depends: libc6 (>= 2.15), libicu57 (>= 57.1-1~), liblzma5 (>= 5.1.1alpha+20120614), zlib1g (>= 1:1.2.3.3)
Recommends: xml-core

Package: linux-libc-dev
Version: 4.7.2-1
Architecture: amd64
Multi-Arch: same
Priority: optional
Source: linux
Provides: linux-kernel-headers

Package: login
Version: 1:4.2-3.1
Architecture: amd64
Multi-Arch: no
Essential: yes
Priority: required
Source: shadow
Pre-Depends: libaudit1 (>= 1:2.2.1), libc6 (>= 2.14), libpam0g (>= 0.99.7.1), libpam-runtime, libpam-modules (>= 1.1.8-1)
Conflicts: amavisd-new (<< 2.3.3-8), backupninja (<< 0.9.3-5), echolot (<< 2.1.8-4), gnunet (<< 0.7.0c-2), python-4suite (<< 0.99cvs20060405-1)
Replaces: manpages-de (<< 0.5-3), manpages-tr (<< 1.0.5), manpages-zh (<< 1.5.1-1)

Package: m4
Version: 1.4.17-5
Architecture: amd64
Multi-Arch: foreign
Priority: optional
Depends: libc6 (>= 2.17), libsigsegv2 (>= 2.9)

Package: make-guile
Version: 4.1-9
Architecture: amd64
Multi-Arch: allowed
Priority: extra
Source: make-dfsg
Provides: make
Depends: guile-2.0-libs, libc6 (>= 2.17), libgc1c2 (>= 1:7.2d)
Conflicts: make
Suggests: make-doc
Replaces: make

Package: man-db
Version: 2.7.5-1
Architecture: amd64
Multi-Arch: foreign
Priority: standard
Provides: man, man-browser
Depends: groff-base (>= 1.18.1.1-15), bsdmainutils, debconf (>= 1.2.0) | debconf-2.0, libc6 (>= 2.17), libgdbm3 (>= 1.8.3), libpipeline1 (>= 1.4.0), zlib1g (>= 1:1.1.4)
Pre-Depends: dpkg (>= 1.16.1~)
Conflicts: man, suidmanager (<< 0.50)
Breaks: manpages-zh (<< 1.5.2-1.1)
Suggests: groff, less, www-browser
Replaces: man, manpages-de (<< 0.5-4), manpages-zh (<< 1.5.2-1.1), nlsutils

Package: mount
Version: 2.28.1-1
Architecture: amd64
Multi-Arch: foreign
Essential: yes
Priority: required
Source: util-linux
Pre-Depends: libblkid1 (>= 2.17.2), libc6 (>= 2.17), libmount1 (>= 2.25), libselinux1 (>= 2.0.15), libsmartcols1 (>= 2.28~rc1), libudev1 (>= 183)
Breaks: bash-completion (<< 1:2.1-4.3~)
Suggests: nfs-common (>= 1:1.1.0-13)
Replaces: bash-completion (<< 1:2.1-4.3~)

Package: multiarch-support
Version: 2.23-5
Architecture: amd64
Multi-Arch: foreign
Priority: required
Source: glibc
Depends: libc6 (>= 2.3.6-2)

Package: ncurses-bin
Version: 6.0+20160625-1
Architecture: amd64
Multi-Arch: foreign
Essential: yes
Priority: required
Source: ncurses
Pre-Depends: libc6 (>= 2.14), libtinfo5 (>= 6.0+20151017), libtinfo5 (<< 6.1~)

Package: passwd
Version: 1:4.2-3.1
Architecture: amd64
Multi-Arch: foreign
Priority: required
Source: shadow
Depends: libaudit1 (>= 1:2.2.1), libc6 (>= 2.14), libpam0g (>= 0.99.7.1), libselinux1 (>= 1.32), libsemanage1 (>= 2.0.3), libpam-modules, debianutils (>= 2.15.2)
Replaces: manpages-tr (<< 1.0.5), manpages-zh (<< 1.5.1-1)

Package: patch
Version: 2.7.5-1
Architecture: amd64
Multi-Arch: foreign
Priority: optional
Depends: libc6 (>= 2.17)
Suggests: ed, diffutils-doc

Package: perl
Version: 5.22.2-3
Architecture: amd64
Multi-Arch: allowed
Priority: standard
Depends: perl-base (= 5.22.2-3), perl-modules-5.22 (>= 5.22.2-3), libperl5.22 (= 5.22.2-3)
Pre-Depends: dpkg (>= 1.17.17)
Conflicts: libjson-pp-perl (<< 2.27200-2)
Breaks: perl-doc (<< 5.22.2-1)
Suggests: perl-doc, libterm-readline-gnu-perl | libterm-readline-perl-perl, make
Recommends: netbase, rename
Replaces: perl-modules (<< 5.22.0~)

Package: perl-base
Version: 5.22.2-3
Architecture: amd64
Multi-Arch: no
Essential: yes
Priority: required
Source: perl
Provides: libfile-path-perl, libfile-temp-perl, libio-socket-ip-perl, libscalar-list-utils-perl, libsocket-perl, libxsloader-perl, perlapi-5.22.1, perlapi-5.22.2
Pre-Depends: libc6 (>= 2.23), dpkg (>= 1.17.17)
Conflicts: defoma (<< 0.11.12), doc-base (<< 0.10.3), mono-gac (<< 2.10.8.1-3), safe-rm (<< 0.8), update-inetd (<< 4.41)
Breaks: autoconf2.13 (<< 2.13-45), backuppc (<< 3.3.1-2), libalien-wxwidgets-perl (<< 0.65+dfsg-2), libanyevent-perl (<< 7.070-2), libcommon-sense-perl (<< 3.72-2~), libfile-path-perl (<< 2.09), libfile-spec-perl (<< 3.5601), libfile-temp-perl (<< 0.2304), libgtk2-perl-doc (<< 2:1.2491-4), libio-socket-ip-perl (<< 0.37), libjcode-perl (<< 2.13-3), libmarc-charset-perl (<< 1.2), libsbuild-perl (<< 0.67.0-1), libscalar-list-utils-perl (<< 1:1.41), libsocket-perl (<< 2.018), libxsloader-perl (<< 0.20), mailagent (<< 1:3.1-81-2), pdl (<< 1:2.007-4), perl (<< 5.22.2~), perl-modules (<< 5.22.2~)
Suggests: perl
Replaces: libfile-path-perl (<< 2.09), libfile-temp-perl (<< 0.2304), libio-socket-ip-perl (<< 0.37), libscalar-list-utils-perl (<< 1:1.41), libsocket-perl (<< 2.018), libxsloader-perl (<< 0.20), perl (<< 5.10.1-12), perl-modules (<< 5.20.1-3)

Package: sed
Version: 4.2.2-7.1
Architecture: amd64
Multi-Arch: foreign
Essential: yes
Priority: required
Depends: dpkg (>= 1.15.4) | install-info
Pre-Depends: libc6 (>= 2.14), libselinux1 (>= 1.32)

Package: sysvinit-utils
Version: 2.88dsf-59.8
Architecture: amd64
Multi-Arch: no
Essential: yes
Priority: required
Source: sysvinit
Depends: libc6 (>= 2.14), init-system-helpers (>= 1.25~), util-linux (>> 2.28-2~)
Breaks: systemd (<< 215)
Replaces: initscripts (<< 2.88dsf-59.5)

Package: tar
Version: 1.29b-1
Architecture: amd64
Multi-Arch: foreign
Essential: yes
Priority: required
Pre-Depends: libacl1 (>= 2.2.51-8), libc6 (>= 2.17), libselinux1 (>= 1.32)
Conflicts: cpio (<= 2.4.2-38)
Breaks: dpkg-dev (<< 1.14.26)
Suggests: bzip2, ncompress, xz-utils, tar-scripts
Replaces: cpio (<< 2.4.2-39)

Package: util-linux
Version: 2.28.1-1
Architecture: amd64
Multi-Arch: foreign
Essential: yes
Priority: required
Pre-Depends: libblkid1 (>= 2.25), libc6 (>= 2.15), libfdisk1 (>= 2.28~rc1), libmount1 (>= 2.25), libncursesw5 (>= 6), libpam0g (>= 0.99.7.1), libselinux1 (>= 1.32), libsmartcols1 (>= 2.28~rc1), libsystemd0, libtinfo5 (>= 6), libudev1 (>= 183), libuuid1 (>= 2.16), zlib1g (>= 1:1.1.4)
Breaks: bash-completion (<< 1:2.1-4.1~), cloud-utils (<< 0.27-1~), grml-debootstrap (<< 0.68), mount (= 2.26.2-3), mount (= 2.26.2-3ubuntu1), sysvinit-utils (<< 2.88dsf-59.4~)
Suggests: dosfstools, kbd | console-tools, util-linux-locales
Replaces: bash-completion (<< 1:2.1-4.1~), initscripts (<< 2.88dsf-59.2~), mount (= 2.26.2-3), mount (= 2.26.2-3ubuntu1), sysvinit-utils (<< 2.88dsf-59.1~)

Package: xz-utils
Version: 5.1.1alpha+20120614-2.1
Architecture: amd64
Multi-Arch: foreign
Priority: standard
Provides: lzma
Depends: libc6 (>= 2.14), liblzma5 (>= 5.1.1alpha+20120614)
Conflicts: lzma (<< 9.22-1), xz-lzma
Breaks: lzip (<< 1.8~rc2)
Replaces: lzip (<< 1.8~rc2), xz-lzma

Package: zlib1g
Version: 1:1.2.8.dfsg-2+b1
Architecture: amd64
Multi-Arch: same
Priority: required
Source: zlib (1:1.2.8.dfsg-2)
Provides: libz1
Depends: libc6 (>= 2.14)
Pre-Depends: multiarch-support
Conflicts: zlib1 (<= 1:1.0.4-7)
Breaks: libxml2 (<< 2.7.6.dfsg-2), texlive-binaries (<< 2009-12)

